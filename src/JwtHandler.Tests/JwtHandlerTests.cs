﻿using Coscine.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.JwtHandler.Tests
{
    [TestFixture]
    public class JwtHandlerTests
    {

        private JWTHandler _jwtHandler;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _jwtHandler = new JWTHandler(new ConsulConfiguration());
        }

        [Test]
        public void CreateAndVlidateTest()
        {
            var type = "TestGuid";
            var guid = Guid.NewGuid().ToString();
            var payload = new Dictionary<string, string> 
            {
                {type, guid }
            };

            var token = _jwtHandler.GenerateJwtToken(payload);
            var contents = _jwtHandler.GetContents(token);

            Assert.True(contents.Count() == 5);
            Assert.True(contents.Where( x => x.Type == type).Count() == 1);
            Assert.True(contents.Where(x => x.Type == type).First().Value == guid);
        }
    }
}
