﻿using Coscine.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Coscine.JwtHandler
{
    public class JWTHandler
    {
        public IConfiguration Configuration { get; set; }
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;
        private readonly SymmetricSecurityKey _symmetricSecurityKey;
        private readonly DateTime _centuryBegin;

        // How long the default token is valid (in minutes).
        private readonly double _defaultExpiration;

        private readonly string _issuer;
        private readonly string _audience;

        public JWTHandler(IConfiguration configuration)
        {
            Configuration = configuration;
            _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            _symmetricSecurityKey = GetSecurityKey();
            _centuryBegin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            _defaultExpiration = 1440;
            _issuer = "https://coscine.rwth-aachen.de";
            _audience = "https://coscine.rwth-aachen.de";
        }

        public SymmetricSecurityKey GetSecurityKey()
        {
            string secretKey = Configuration.GetStringAndWait("coscine/global/jwtsecret");

            if (secretKey == null)
            {
                throw new ArgumentNullException("JWT Secret Configuration value is not set!");
            }

            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
        }

        public bool IsTokenValid(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidAudience = _audience,
                ValidIssuer = _issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _symmetricSecurityKey,
                ValidateIssuer = false,
                ValidateAudience = false,
                // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                ClockSkew = TimeSpan.Zero
            };

            _jwtSecurityTokenHandler.ValidateToken(token, tokenValidationParameters, out _);

            return true;
        }

        public bool TryValidateToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidAudience = _audience,
                ValidIssuer = _issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _symmetricSecurityKey,
                ValidateIssuer = false,
                ValidateAudience = false,
                // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                ClockSkew = TimeSpan.Zero
            };

            try
            {
                _jwtSecurityTokenHandler.ValidateToken(token, tokenValidationParameters, out _);
            }
            catch (Exception _)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<Claim> GetContents(string jwt)
        {
            return _jwtSecurityTokenHandler.ReadJwtToken(jwt).Claims;
        }

        public string GenerateJwtToken(JwtPayload payload, string signatureAlgorithm = "HS256")
        {
            var issuedAt = DateTime.UtcNow;
            var expires = issuedAt.AddMinutes(_defaultExpiration);
            return GenerateJwtToken(payload, _issuer, _audience, issuedAt, expires, signatureAlgorithm);
        }

        public string GenerateJwtToken(JwtPayload payload, string issuer, string audience, DateTime issuedAt, DateTime expires, string signatureAlgorithm = "HS256")
        {
            if (payload == null)
            {
                throw new ArgumentNullException("JwtPayload value is not set!");
            }

            var signingCredentials = new SigningCredentials(_symmetricSecurityKey, signatureAlgorithm);

            var exp = (expires - _centuryBegin).TotalSeconds;
            var iat = (issuedAt - _centuryBegin).TotalSeconds;

            payload.Add("iss", issuer);
            payload.Add("aud", audience);
            payload.Add("iat", (long)iat);
            payload.Add("exp", (long)exp);

            var header = new JwtHeader(signingCredentials);
            var securityToken = new JwtSecurityToken(header, payload);

            return _jwtSecurityTokenHandler.WriteToken(securityToken);
        }

        public string GenerateJwtToken(IReadOnlyDictionary<string, string> payloadContents, string signatureAlgorithm = "HS256")
        {
            var payload = new JwtPayload(payloadContents.Select(c => new Claim(c.Key, c.Value)));

            return GenerateJwtToken(payload, signatureAlgorithm);
        }

        public string GenerateJwtToken(IReadOnlyDictionary<string, string> payloadContents, string issuer, string audience, DateTime issuedAt, DateTime expires, string signatureAlgorithm = "HS256")
        {
            var payload = new JwtPayload(payloadContents.Select(c => new Claim(c.Key, c.Value)));

            return GenerateJwtToken(payload, issuer, audience, issuedAt, expires, signatureAlgorithm);
        }
    }
}